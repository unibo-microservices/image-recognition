from array import ArrayType
from multiprocessing.dummy import Array
from flask             import Flask, request
import numpy as np
import joblib
import pickle
import json
import datetime

# These are the possible categories (classes) which can be detected
namemap = [
    'axes',
    'boots',
    'carabiners',
    'crampons',
    'gloves',
    'hardshell_jackets',
    'harnesses',
    'helmets',
    'insulated_jackets',
    'pulleys',
    'rope',
    'tents'
]

app = Flask(__name__)



@app.route('/classifier', methods=['POST'])
def post_classify():
    try:
        start = datetime.datetime.now()
        # Get the processed image array data
        data = request.json
        
        imgFeatures = np.array(data['imgfeatures'])
        
        print(imgFeatures)
        
        # Model
        model = joblib.load('pickle_model.pkl')

        # Prediction
        predict = model.predict(imgFeatures)
        
        # Results
        print('The image is a ', namemap[int(predict[0])]),
        print('image: ', namemap[int(predict[0])], predict)
        
        # Convert the integer returned from the model into the name of the class from our namemap above.
        response = json.dumps({"classification": namemap[int(predict[0])]})
        
        stop = datetime.datetime.now()
        execution = stop - start
        print("MS2: Model Loading + Prediction + Results in microseconds: ", execution.microseconds)
        
        # response = json.dumps({"classification":"tents"}) #TEST
        return(response)

    except Exception as e:
        print(e)
        raise

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5004)
