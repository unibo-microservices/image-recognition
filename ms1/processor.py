# import zlib
import datetime
import requests
import numpy as np
from PIL import Image, ImageOps 
# from base64 import b64decode
from flask import Flask, json, request
from json import JSONEncoder


app = Flask(__name__)

def resize(image):
    # Resize of the image (128x128) (it depends by the used model)
    
    base = 128
    width, height = image.size

    if width > height:
        wpercent = base/float(width)
        hsize    = int((float(height) * float(wpercent)))
        image    = image.resize((base,hsize), Image.ANTIALIAS)
    else:
        hpercent = base/float(height)
        wsize    = int((float(width) * float(hpercent)))
        image    = image.resize((wsize, base), Image.ANTIALIAS)

    newImage = Image.new('RGB',
                     (base, base),     # A4 at 72dpi
                     (255, 255, 255))  # White

    position = (int( (base/2 - image.width/2) ), 0)
    newImage.paste(image, position)

    return newImage


def normalize(arr):
    """ This means that the largest value for each attribute is 1 and the smallest value is 0.
        Normalization is a good technique to use when you do not know the distribution of your data 
        or when you know the distribution is not Gaussian (a bell curve)."""
    
    arr = arr.astype('float')
    
    # Do not touch the alpha channel
    for i in range(3):
        minval = arr[...,i].min()
        maxval = arr[...,i].max()
        if minval != maxval:
            arr[...,i] -= minval
            arr[...,i] *= (255.0/(maxval-minval))

    return arr

@app.route('/processor', methods=['GET']) # POST con ms0
def processImage():
    start = datetime.datetime.now()

    image = Image.open('test-image.png')

    # Resize + Normalize
    img   = resize(image)
    arr     = np.array(img)
    new_img = Image.fromarray(normalize(arr).astype('uint8'),'RGB')
    
    # Convert a 2D image into a flat array
    imgFeatures = np.array(new_img).ravel().reshape(1,-1)
    print(imgFeatures)
    
    stop = datetime.datetime.now()
    execution = stop - start
    print("MS1: Image Resize + Normalize + Convert into a flat array time in microseconds: ", execution.microseconds)
    
    r = requests.post(url="http://ms2:5004/classifier", json={'arraytype':"flat",'imgfeatures': imgFeatures.tolist()})
    data = r.json()
    stop = datetime.datetime.now()
    execution = stop - start
    ex_time = execution.total_seconds() * 1000
    print("MS1+MS2+MS1: Total process+classify requests time in ms: ", ex_time)
    return json.dumps(data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5003)
